/*
 * Copyright (C) 2023  Maciej Sopylo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * systemd-service-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

import Example 1.0

MainView {
  id: root
  objectName: 'mainView'
  applicationName: 'systemd-service-demo.klh'
  automaticOrientation: true

  width: units.gu(45)
  height: units.gu(75)

  Page {
    anchors.fill: parent

    header: PageHeader {
      id: header
      title: i18n.tr('Systemd Service Demo')
    }

    ColumnLayout {
      spacing: units.gu(2)

      anchors {
        margins: units.gu(2)
        top: header.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
      }

      Item {
        Layout.fillHeight: true
      }

      Label {
        Layout.fillWidth: true
        text: i18n.tr('You can verify these are working over ssh/adb using "systemctl --user status" and "systemctl --user is-enabled"')
        wrapMode: Text.Wrap
      }

      Button {
        Layout.alignment: Qt.AlignHCenter
        text: i18n.tr('Stop mediascanner-2.0')
        onClicked: Example.stop("mediascanner-2.0.service")
      }

      Button {
        Layout.alignment: Qt.AlignHCenter
        text: i18n.tr('Start mediascanner-2.0')
        onClicked: Example.start("mediascanner-2.0.service")
      }

      Button {
        Layout.alignment: Qt.AlignHCenter
        text: i18n.tr('Restart mediascanner-2.0')
        onClicked: Example.restart("mediascanner-2.0.service")
      }

      Button {
        Layout.alignment: Qt.AlignHCenter
        text: i18n.tr('Disable gnome-keyring')
        onClicked: Example.disable("gnome-keyring.service")
      }

      Button {
        Layout.alignment: Qt.AlignHCenter
        text: i18n.tr('Enable gnome-keyring')
        onClicked: Example.enable("gnome-keyring.service")
      }

      Item {
        Layout.fillHeight: true
      }
    }
  }
}
