/*
 * Copyright (C) 2023  Maciej Sopylo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * systemd-service-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>

#include "example.h"

void Example::stop(QString unit) {
  callDBus("StopUnit", unit, "fail");
}

void Example::start(QString unit) {
  callDBus("StartUnit", unit, "fail");
}

void Example::restart(QString unit) {
  callDBus("RestartUnit", unit, "fail");
}

void Example::enable(QString file) {
  QStringList unitFiles = { file };
  callDBus("EnableUnitFiles", unitFiles, false, false);
}

void Example::disable(QString file) {
  QStringList unitFiles = { file };
  callDBus("DisableUnitFiles", unitFiles, false);
}
