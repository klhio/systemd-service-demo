/*
 * Copyright (C) 2023  Maciej Sopylo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * systemd-service-demo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXAMPLE_H
#define EXAMPLE_H

#define SERVICE_NAME "org.freedesktop.systemd1"
#define DBUS_PATH "/org/freedesktop/systemd1"
#define DBUS_INTERFACE "org.freedesktop.systemd1.Manager"

#include <QObject>
#include <QtDBus/QtDBus>

class Example: public QObject {
  Q_OBJECT

public:
  ~Example() = default;

  Q_INVOKABLE void stop(QString unit);
  Q_INVOKABLE void start(QString unit);
  Q_INVOKABLE void restart(QString unit);
  Q_INVOKABLE void enable(QString file);
  Q_INVOKABLE void disable(QString file);
private:
  template <typename...Args> void callDBus(const QString &method, Args &&...args);
};

template <typename...Args> void Example::callDBus(const QString &method, Args &&...args) {
  if (!QDBusConnection::sessionBus().isConnected()) {
    qDebug() << "can't connect to session bus!";
    return;
  }

  QDBusInterface iface(SERVICE_NAME, DBUS_PATH, DBUS_INTERFACE, QDBusConnection::sessionBus());
  if (!iface.isValid()) {
    qDebug() << "interface invalid" << QDBusConnection::sessionBus().lastError().message();
    return;
  }

  QDBusMessage reply = iface.call(method, args...);
  if (reply.type() == QDBusMessage::ReplyMessage) {
    qDebug() << "calling" << method << "successful";
  } else {
    qDebug() << "calling" << method << "failed:" << reply.errorMessage();
  }
}

#endif
