# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the systemd-service-demo.klh package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: systemd-service-demo.klh\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-03 00:05+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:39 systemd-service-demo.desktop.in.h:1
msgid "Systemd Service Demo"
msgstr ""

#: ../qml/Main.qml:59
msgid ""
"You can verify these are working over ssh/adb using \"systemctl --user status"
"\" and \"systemctl --user is-enabled\""
msgstr ""

#: ../qml/Main.qml:65
msgid "Stop mediascanner-2.0"
msgstr ""

#: ../qml/Main.qml:71
msgid "Start mediascanner-2.0"
msgstr ""

#: ../qml/Main.qml:77
msgid "Restart mediascanner-2.0"
msgstr ""

#: ../qml/Main.qml:83
msgid "Disable gnome-keyring"
msgstr ""

#: ../qml/Main.qml:89
msgid "Enable gnome-keyring"
msgstr ""
